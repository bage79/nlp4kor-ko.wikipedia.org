# License
- [CC BY-SA License](http://creativecommons.org/licenses/by-sa/3.0/deed.ko)
  - wikipedia의 라이센스가 상속됩니다. [CCL License](http://creativecommons.org/licenses/by-sa/3.0/deed.ko)
  - 무료 또는 상용 소프트웨어로 배포하시는 경우, 데이터파일이 있는 디렉터리에 아래 파일들을 포함하셔야 합니다.
    - LICENSE (라이센스 표시)
    - ko.wikipedia.org.urls.txt (출처 표시)
