### Docs
- total docs: 518,062
- total docs: 249,791 (the sentences which has hangul characters.)

### Sentences
- total sentences: 3,115,431 (the sentences which has hangul characters.)
- NE sentences: 1,788,897 (has NE)
- train sentences: 2,683,517
- valid sentences: 215,957
- test sentences: 215,957

### Words
- 2,205,841 unique words (min count:2)

### Characters
- total characters: 15,232
