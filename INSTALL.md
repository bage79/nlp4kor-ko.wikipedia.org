### install git & git lfs
    - https://github.com/git-lfs/git-lfs/wiki/Installation
    - SourceTree 등의 tool을 이용하시면, git lfs 파일의 clone이 쉽습니다.
```shell
# OSX 기준 (최초 설치)
brew install git git-lfs
git lfs install
```